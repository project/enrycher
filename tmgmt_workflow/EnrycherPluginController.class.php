<?php

class EnrycherPluginController extends WorkflowTranslationDefaultService {

  /**
   * Start the Enrichment process.
   *
   * @param TMGMTJob $job
   *   The current JobItem, which should be enriched.
   *
   * @return bool
   *   Returns true, after the enrichment is finished.
   */
  public function start(TMGMTJob $job) {
    $items = $job->getItems();
    foreach ($items as $job_item) {
      /** @var $job_item TMGMTJobItem */
      if (in_array($job_item->plugin, array('node', 'entity'))) {
        $entity = entity_load($job_item->item_type, array($job_item->item_id));
        $entity = reset($entity);
        $data = $job_item->getData();

        // Do not enrich node title because html is not allowed in title.
        if (!empty($data['node_title'])) {
          $data['node_title']['#translate'] = FALSE;
        }
        $all_data = $this->enrichData($data, $job->source_language);

        // Create new revision with enriched data.
        $entity->revision = 1;
        $entity->is_new_revision = TRUE;
        $entity->log = "Automatically created by Enrycher API.";
        switch ($job_item->plugin) {
          case 'node':
            tmgmt_node_update_node_translation($entity, $all_data, $job->source_language);
            break;

          case 'entity':
            tmgmt_entity_update_entity_translation($entity, $job_item->item_type, $all_data, $job->source_language);
            break;
        }
        unset($all_data['node_title']['#translate']);

        // Save new data to job_item.
        $job_item->data = $all_data;
        $job_item->save();
      }
    }
    return TRUE;
  }

  /**
   * Enrich a TMGMT Job Item data array with the enrycher API.
   *
   * @param array $data
   *   TMGMT Job Item data array.
   * @param string $source_lang
   *   Source language language code..
   *
   * @return string
   *   Enriched data array.
   */
  public function enrichData($data, $source_lang) {
    // Text shouldn't be empty and not marked as not to translated.
    if (!empty($data['#text'])) {

      if ((!isset($data['#translate']) || $data['#translate'])) {
        $response = enrycher_enrich_data($data['#text'], $source_lang);
        if ($response) {
          $data['#text'] = $response;
        }
      }
    }

    foreach (element_children($data) as $key) {
      $data[$key] = $this->enrichData($data[$key], $source_lang);
    }

    return $data;
  }

  /**
   * Returns an array with supported languages for a translation workflow.
   *
   * Enrycher API works for English and Slovenia. As it works with the source
   * and doesn't do any translation, this function returns all possible
   * languages, if the source is English or Slovenia.
   *
   * @param string $source_language
   *   The source language code.
   *
   * @return array
   *   List of supported languages.
   */
  public function getSupportedTargetLanguages($source_language) {
    if (in_array($source_language, array('en', 'si'))) {
      $languages = entity_metadata_language_list();
      unset($languages[LANGUAGE_NONE], $languages[$source_language]);
      return drupal_map_assoc(array_keys($languages));
    }
  }

}
