SUMMARY
=======

Integration for the Enrycher service to automatically add Term Analysis data
from the International Tag Set 2.0 to the content. This module adds a button in
the WYSIWYG editor to automatically enrich you data, therefore the WYSIWYG
module is required. It is recommended to have the ITS Integration module
enabled, but not required. If enabled, it adds the button for automatic
enrichment to the Language Management from the ITS module.

Also it provides a TMGMT Workflow service. With this the enrichment can be done
before an translation is sent to the LSP with TMGMT.

You can find more information about the Enrycher at
http://ailab.ijs.si/tools/enrycher/.

REQUIREMENTS
============

WYSIWYG (wysiwyg)
  http://drupal.org/project/wysiwyg

OPTIONAL (but recommended)
--------------------------

Internation Tag Set 2.0 Integration (its)
  http://drupal.org/project/its

TMGMT Workflow (tmgmt_workflow), when using TMGMT
  http://drupal.org/sandbox/kfritsche/1908598

INSTALLATION
============

* Install the required modules as usual (http://drupal.org/node/70151).

* Install this module as usual, see http://drupal.org/node/70151
  for further information.

* Enable Enrycher Plugin in the WYSIWYG settings.
  e.g. http://your-domain.com/admin/config/content/wysiwyg/profile/full_html/edit
    under "Buttons and Plugins" enable Enrycher Service.

CONFIGURATION
=============

* Configure user permissions in Administration -> People -> Permissions:

  - Access Enrycher API (access enrycher)

    Allow a user to use the Enrycher API.

* Enable Enrycher Plugin in the WYSIWYG settings.

CONTACT
=======

Current maintainer:
* Karl Fritsche (kfritsche) - http://drupal.org/user/619702

This project has been sponsored by:
* Cocomore AG
  http://www.cocomore.com

* MultilingualWeb-LT Working Group
  http://www.w3.org/International/multilingualweb/lt/

* European Commission - CORDIS - Seventh Framework Programme (FP7)
  http://cordis.europa.eu/fp7/dc/index.cfm
