/**
 * Sends content from ITS Language Management to enrycher.
 * @file
 */

(function ($) {

  Drupal.behaviors.its_text_enrycher = {

    running: false,

    /**
     * Initial attach function.
     *
     * @param context
     * @param settings
     */
    attach: function (context, settings) {
      var enrycher = this;

      $('.form-item-checkbox-enrycher label a:not(.enrycher-processed)', context).each(function() {

        var link = $(this);
        link.addClass('.enrycher-processed');
        link.click(function () {

          enrycher.running = true;

          $('body').append('<div id="enrycher-progress-thobber"><span>' + Drupal.t('Please wait, request will be fullfilled.') + '</span></div>');

          var $fullText = $('<div>');

          $('.field-its-edit-text', context).each(function (key, text) {
            var $text = $(text),
              parentID = $text.parent().attr('id');
            $('<div>').attr('id', parentID).addClass('enrycher-processed').html($text.html()).appendTo($fullText);
          });

          var ajaxOptions = {
            type: 'POST',
            dataType: 'json',
            url: Drupal.settings.basePath + (Drupal.settings.pathPrefix ? Drupal.settings.pathPrefix : '' ) + 'enrycher/js',
            success: enrycher.handleSuccess,
            complete: enrycher.handleComplete,
            data: {
              language: Drupal.settings.enrycher.language,
              text: $fullText.html()
            }
          };

          $.ajax(ajaxOptions);

        });
      });

    },

    /**
     * Success handler for ajax request.
     * @param data
     * @param textStatus
     * @param xhr
     */
    handleSuccess: function (data, textStatus, xhr) {
      if (data.text) {

        $(data.text).each(function(key, text) {
          var $text = $(text),
            id = $text.attr('id');
          $('.field-its-edit-text', '#' + id).html($text.html());
        });

        // Update text analysis highlight.
        var index = Drupal.its.filter.activeCategories.indexOf('its_textAnalysis');
        if (index != -1) {
          var $texts = $('.field-its-edit-text');
          $texts.each(function () {
            Drupal.its.filter.hide('its_textAnalysis', this);
          });
          delete Drupal.its.filter.activeCategories[index];
          $texts.each(function () {
            Drupal.its.filter.show('its_textAnalysis', this);
          });
          Drupal.its.filter.activeCategories.push('its_textAnalysis');
        }
      }
    },

    /**
     * Is executed after every request, ignoring status of return.
     * @param xhr
     * @param textStatus
     */
    handleComplete: function (xhr, textStatus) {
      var enrycher = Drupal.behaviors.its_text_enrycher;
      enrycher.running = false;
      $('#enrycher-progress-thobber').remove();
    }

  }

}(jQuery));
