/**
 * @file
 * Implementation file for the WYSIWYG Plugin of enrycher integration.
 */

(function ($) {

  Drupal.wysiwyg.plugins.enrycher = {

    running: false,
    instanceId: null,

    /**
     * Checks if the given node is already enriched.
     * As its a automatic service this always return false.
     * @param node
     */
    isNode: function(node) {
      return this.running;
    },

    /**
     * Enrich the selected data and reintegrate it to the wysiwyg.
     * @param data
     * @param settings
     * @param instanceId
     */
    invoke: function(data, settings, instanceId) {
      // No parallel requests allowed.
      if (this.running) return;
      this.running = true;
      this.instanceId = instanceId;

      $('body').append('<div id="enrycher-progress-thobber"><span>' + Drupal.t('Please wait, request will be fullfilled.') + '</span></div>');

      var ajaxOptions = {
        type: 'POST',
          dataType: 'json',
          url: Drupal.settings.basePath + (Drupal.settings.pathPrefix ? Drupal.settings.pathPrefix : '' ) + 'enrycher/js',
          success: this.handleSuccess,
          complete: this.handleComplete
      };

      var languageField = $('#edit-language'),
        lang = null,
        text;

      if (languageField.size() > 0) {
        lang = languageField.val();
      }

      text = Drupal.wysiwyg.instances[instanceId].getContent();

      if (typeof text != 'undefined') {
        ajaxOptions.data = {text: text, lang: lang};
        $.ajax(ajaxOptions);
      }
    },

    /**
     * Success handler for ajax request.
     * @param data
     * @param textStatus
     * @param xhr
     */
    handleSuccess: function(data, textStatus, xhr) {
      var enrycher = Drupal.wysiwyg.plugins.enrycher;
      console.log(data, enrycher);
      if (enrycher.instanceId && data.text) {
        Drupal.wysiwyg.instances[enrycher.instanceId].setContent(data.text)
      }
    },

    /**
     * Is executed after every request, ignoring status of return.
     * @param xhr
     * @param textStatus
     */
    handleComplete: function(xhr, textStatus) {
      var enrycher = Drupal.wysiwyg.plugins.enrycher;
      enrycher.running = false;
      enrycher.instanceId = null;
      $('#enrycher-progress-thobber').remove();
    }

  }

}(jQuery));
