<?php

/**
 * @file
 * WYSIWYG file to include Enrycher there.
 */

/**
 * Define a WYSIWYG plugin, which allows to run the Enrycher API from there.
 *
 * @return array
 *   Meta information about the buttons provided by this plugin.
 */
function enrycher_enrycher_plugin() {
  $plugins = array();

  $plugins['enrycher'] = array(
    'title' => t('Enrycher Service'),
    'vendor url' => 'http://drupal.org/project/enrycher',
    'icon path' => drupal_get_path('module', 'enrycher') . '/wysiwyg',
    'icon file' => 'enrycher.png',
    'icon title' => t('Automatically enrich data with text analysis data category.'),
    'settings' => array(
      'validAttributes' => array(
        'its-ta-confidence',
        'its-ta-class-ref',
        'its-ta-source',
        'its-ta-ident-ref',
        'its-ta-annotators-ref',
      ),
    ),
    'buttons' => array('enrycher' => t('text analysis')),
    'load' => TRUE,
    'internal' => FALSE,
    'js path' => drupal_get_path('module', 'enrycher') . '/wysiwyg',
    'js file' => 'enrycher.wysiwyg.js',
    'css path' => drupal_get_path('module', 'enrycher') . '/wysiwyg',
    'css file' => 'enrycher.wysiwyg.js',
  );

  return $plugins;
}
